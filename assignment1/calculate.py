import logging
from math import e, isclose

from pylab import plot, grid, show, style, subplot, title, xlabel, figure, subplots_adjust, tight_layout
from numpy import linspace, log, array, arange

from methods import bisect, fixedpoint, newtonraph
from plot import plot_newton, plot_errors, plot_errorquotas, plot_bisect, plot_fixedpoint


def calculate_errors(f, steps):
    # For a function which we should find zero for
    return [f(step) for step in steps]


def calculate_errorquotients(errors):
    return [errors[i]/errors[i-1] for i in range(1, len(errors))]


def calculate_k_using_bisect(plot_result=False):
    def f(k):
        return (10 + 1/(2*k))*e**(-k) - 7 - 1/(2*k)
    k, steps = bisect(f, -1, 2, e=10 ** -2)
    logging.info("k from bisect: " + str(k))


def calculate_k_using_fixedpoint(plot_result=False):
    def f(k):
        return -log((7+1/(2*k))/(10 + 1/(2*k)))
    k, steps = fixedpoint(f, 0.5, e=10 ** -6)
    logging.info("k from fixedpoint: " + str(k))


def calculate_k_using_newton(plot_result=False):
    def f(k):
        return (10 + 1/(2*k))*e**(-k) - 7 - 1/(2*k)
    k, steps = newtonraph(f, 0.5)
    logging.info("k from newton: " + str(k))
    return k


def calculate_k(plot_result=False):
    logging.info("Calculating k...")
    calculate_k_using_bisect(plot_result)
    calculate_k_using_fixedpoint(plot_result)
    k = calculate_k_using_newton(plot_result)
    return k


temp_at_death = 37


def T(t, k):
    return 22 + 0.5*t - 1/(2*k) + (10 + 1/(2*k))*e**(-k*t) - temp_at_death


def calculate_t_using_newton(k, x_start=-2, plot_result=False):
    T_t = lambda t: T(t, k)
    td, steps = newtonraph(T_t, x_start, eres=True)
    logging.info("td from newton: {} {}".format(td, T(td, k)))
    errors = calculate_errors(T_t, steps)
    errorquotients = calculate_errorquotients(errors)

    if plot_result:
        subplot(2, 1, 1)
        plot_newton(T_t, steps)

        subplot(2, 2, 3)
        plot_errors(errors, logarithmic=True)

        subplot(2, 2, 4)
        plot_errorquotas(errorquotients)

        tight_layout()
        show()

    return td


def calculate_t_using_bisect(k, x_start=-5, x_end=0, plot_result=False):
    T_t = lambda t: T(t, k)
    td, steps = bisect(T_t, x_start, x_end, eres=True)
    logging.info("td from bisect: {} {}".format(td, T(td, k)))
    errors = calculate_errors(T_t, steps)
    errorquotients = [errors[i]/errors[i-1] for i in range(1, len(errors))]

    if plot_result:
        subplot(2, 1, 1)
        plot_bisect(T_t, steps)

        subplot(2, 2, 3)
        plot_errors(errors, logarithmic=True)

        subplot(2, 2, 4)
        plot_errorquotas(errorquotients)

        tight_layout()
        show()

    return td


def calculate_t_using_fixedpoint(k, x_start=-1, plot_result=False):
    T_t = lambda t: T(t, k)
    Tmod = lambda t: -log(-(-temp_at_death + 22 + 0.5*t - 1/(2*k)) / (10 + 1/(2*k))) / k
    td, steps = fixedpoint(lambda t: Tmod(t), x_start, eres=True)
    logging.info("td from fixedpoint: {} {}".format(td, Tmod(td)))
    errors = calculate_errors(T_t, steps)
    errorquotients = calculate_errorquotients(errors)

    if plot_result:
        subplot(2, 1, 1)
        plot_fixedpoint(Tmod, steps)

        subplot(2, 2, 3)
        plot_errors(errors, logarithmic=True)

        subplot(2, 2, 4)
        plot_errorquotas(errorquotients)

        tight_layout()
        show()

    return td


def calculate_t(k, plot_result=False):
    logging.info("Calculating t...")
    x_start = -5
    x_end = 0

    td1 = calculate_t_using_newton(k, x_start=x_start, plot_result=plot_result)
    td2 = calculate_t_using_bisect(k, x_start=x_start, x_end=x_end, plot_result=plot_result)
    td3 = calculate_t_using_fixedpoint(k, x_start=x_start, plot_result=plot_result)

    assert isclose(td1, td2, rel_tol=10**-5)
    assert isclose(td1, td3, rel_tol=10**-5)

    return td1

