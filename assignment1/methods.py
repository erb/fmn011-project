from typing import Callable, List
import logging

from scipy.misc import derivative

from errors import *

ITERMAX = 10000
ERROR = 10 ** -8


def bisect(f: Callable[[float], float], a: float, b: float,
           e: float=ERROR, itermax: int=ITERMAX, eres: bool=False) \
        -> (float, List[float]):
    """
    :param f: The function to find x for where `abs(f(x)) < e`
    :param a: The left boundary of the interval which x should be within
    :param b: The right boundary of the interval which x should be within
    :param e: The error margin which the result must be within for
              the solution to be considered found
    :param itermax: The maximum number of iterations to try and find
                    x in before giving up
    :param eres: Decides whether the maximum error for the stopping criteria
                 should be measured as abs(f(p_n)) or abs(p_n - p_(n-1))
    :return: If convergent the x for which `f(x) < e` holds true along with
             a list of the intermediate steps
    """
    steps = [a, b]
    error = float('infinity')

    while len(steps)-2 < itermax and error > e:
        assert f(a) * f(b) < 0
        c = (a + b) / 2
        steps.append(c)
        fc = f(c)

        if not eres:
            error = abs(steps[-1] - steps[-2])
        else:
            error = abs(fc)

        if f(a) * fc > 0:
            a = c
        else:
            b = c

    logging.info("Bisection completed in {} iterations with error: {} {}"
                 .format(len(steps), absolute_error(steps[-1], steps[-2]),
                         relative_error(steps[-1], steps[-2])))
    return c, steps


def fixedpoint(f: Callable[[float], float], x: float,
               e: float=ERROR, itermax: int=ITERMAX, eres=False)\
        -> (float, List[float]):
    """
    :param f: The function to find x for
    :param x: The starting x of iteration
    :param e: The error margin which the result must be within for the
              solution to be considered found
    :param itermax: The maximum number of iterations to try and find x
                    in before giving up
    :param eres: Decides whether the maximum error for the stopping criteria
                 should be measured as abs(f(p_n)) or abs(p_n - p_(n-1))
    :return: If convergent the x for which `f(x) < e` holds true along with
             a list of the intermediate steps
    """
    steps = [x]
    error = float('infinity')

    while len(steps) - 1 < itermax and error > e:
        y = f(x)
        steps.append(y)
        if not eres:
            error = abs(y - x)
        else:
            error = abs(y - f(y))

        x = y

    logging.info("Fixed point completed in {} iterations with errors abs: {}, rel: {}"
                 .format(len(steps) - 1, absolute_error(steps[-1], steps[-2]),
                         relative_error(steps[-1], steps[-2])))
    return x, steps


def newtonraph(f: Callable[[float], float], x: float,
               e: float=ERROR, itermax: int=ITERMAX, eres: bool=False)\
        -> (float, List[float]):
    """
    :param f: The function to find x for where `f(x) < e`
    :param x: The starting point
    :param e: The maximum error that is allowed before iteration can stop
    :param itermax: The maximum number of iterations to try and find x in
                    before giving up
    :param eres: Decides whether the maximum error for the stopping criteria
                 should be measured as abs(f(p_n)) or abs(p_n - p_(n-1))
    :return: If convergent the x for which `f(x) < e` holds true along with
             a list of the intermediate steps
    """
    steps = [x]
    error = float('infinity')

    while len(steps) - 1 < itermax and error > e:
        last_x = steps[-1]
        dfx = derivative(f, last_x, dx=10**-6)
        x = last_x - f(last_x) / dfx
        steps.append(x)
        if not eres:
            error = abs(x - last_x)
        else:
            error = abs(f(x))

    logging.info("Newton-Raphson completed in {} iterations with errors abs: {}, rel: {}"
                 .format(len(steps) - 1, absolute_error(steps[-1], steps[-2]),
                         relative_error(steps[-1], steps[-2])))
    return x, steps
