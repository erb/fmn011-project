import logging

from calculate import calculate_k, calculate_t

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    k = calculate_k()
    t = calculate_t(k, plot_result=True)
    print("t = {}".format(t))
