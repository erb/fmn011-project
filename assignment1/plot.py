from pylab import plot, grid, show, style, subplot, title, xlabel, autoscale, yscale, ylabel
from numpy import linspace, log, array, arange

style.use('ggplot')


def _calculate_margin(steps):
    return 0.1 * abs(min(steps) - max(steps))


def _calculate_plot_x_boundaries(steps):
    margin = _calculate_margin(steps)
    plot_min_x = min(steps) - margin
    plot_max_x = max(steps) + margin
    return plot_min_x, plot_max_x


def _plot_helper(f, steps,
                 intersect_line=([], []),
                 endpoints=([], [])):
    # Curve of function which we perform numerical analysis on
    plot_min_x, plot_max_x = _calculate_plot_x_boundaries(steps)
    x = linspace(plot_min_x, plot_max_x, 100)
    y = f(x)
    plot(x, y)

    # Plot line which function should intersect with
    plot(*intersect_line, 'k')

    # Plot steps
    xs = steps
    ys = f(array(steps))
    plot(xs[:-1], ys[:-1], 'bo')

    # Plot endpoint(s)
    plot(*endpoints, 'ro')

    # Plot final step
    plot(xs[-1], ys[-1], 'go')


def plot_bisect(f, steps):
    plot_min_x, plot_max_x = _calculate_plot_x_boundaries(steps)

    title("Bisection iterations")
    xlabel("t")
    ylabel("T(t)")
    autoscale(tight=True)

    endpoints = [steps[0], steps[1]], [f(steps[0]), f(steps[1])]
    intersect_line = [plot_min_x, plot_max_x], [0, 0]
    _plot_helper(f, steps,
                 intersect_line=intersect_line,
                 endpoints=endpoints)


def plot_fixedpoint(f, steps):
    plot_min_x, plot_max_x = _calculate_plot_x_boundaries(steps)
    x = linspace(plot_min_x, plot_max_x, 100)

    title("Fixed point iteration")
    xlabel("t")
    ylabel("Tmod(t)")
    # Tmod(t): The modification of T(t) which fulfills Tmod(t) = t
    autoscale(tight=True)

    intersect_line = x, x
    endpoints = steps[0], f(steps[0])
    _plot_helper(f, steps,
                 intersect_line=intersect_line,
                 endpoints=endpoints)


def plot_newton(f, steps):
    plot_min_x, plot_max_x = _calculate_plot_x_boundaries(steps)

    title("Newton-Raphson iteration")
    xlabel("t")
    ylabel("T(t)")
    autoscale(tight=True)

    intersect_line = [plot_min_x, plot_max_x], [0, 0]
    endpoints = steps[0], f(steps[0])
    _plot_helper(f, steps,
                 intersect_line=intersect_line,
                 endpoints=endpoints)


def plot_errors(errors, logarithmic=False):
    title("Absolute error")
    xlabel("Iterations")
    if logarithmic:
        yscale("log")
    plot(arange(len(errors)), [abs(error) for error in errors])


def plot_errorquotas(errorquotas):
    title("Error quotients")
    xlabel("Iterations")
    plot(arange(len(errorquotas)), errorquotas)

