
def absolute_error(x1, x2):
    return abs(x1-x2)


def relative_error(x1, x2):
    # Another method would be:
    #   rel_err = absolute_error(x1, x2) / (2 * (abs(x1) + abs(x2)))
    # However, we cannot guarantee that the error isn't higher than that,
    # we can however guarantee exactly that with the code below.
    return absolute_error(x1, x2)/min(abs(x1), abs(x2))
