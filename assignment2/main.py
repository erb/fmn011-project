from casteljau import casteljau
import pylab as plt

plt.style.use('ggplot')

if __name__ == "__main__":
    max = 100
    cp = [[500, 100], [100, 100], [200, 500], [500, 500]]
    x = [0 for x in range(max)]
    y = [0 for x in range(max)]
    for t in range(max):
        a = casteljau(cp, t/max)
        x[t] = a[0]
        y[t] = a[1]
    print("X equals {}, y equals {}".format(x, y))
    plt.plot(x, y)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.show()
