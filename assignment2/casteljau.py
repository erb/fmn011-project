import numpy as np


def casteljau(b_0, t0):
    n = len(b_0)
    b = []
    for i in range(0, n):
        b.append([np.array(b_0[i])])
    for j in range(1, n):
        for i in range(n - j):
            e1 = (1 - t0) * b[i][j - 1]
            e2 = t0 * b[i + 1][j - 1]
            b[i].append(e1 + e2)
    return b[0][n - 1]
