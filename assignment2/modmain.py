from casteljau import casteljau
import pylab as plt
import numpy as np

if __name__ == "__main__":
    max = 3
    initmax = max
    cp1 = [500, 100], [300, 300], [500, 600]
    cp2 = [500, 600], [500, 500]
    cp3 = [500, 500], [400, 300], [500, 200]
    cp4 = [500, 200], [500, 100]
    cp = []
    cp.append([np.array(cp1)])
    cp.append([np.array(cp2)])
    cp.append([np.array(cp3)])
    cp.append([np.array(cp4)])
    x = [0 for x in range(4*max-1)]
    y = [0 for x in range(4*max-1)]

    for i in range(3):
        for t in range(initmax*i, max-1):
            print(cp[i])
            a = casteljau(cp[i], t/max)
            x[t] = a[0]
            y[t] = a[1]
        max += initmax

    print("X equals {}, y equals {}".format(x, y))
    plt.plot(x, y)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.show()
