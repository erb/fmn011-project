# Takes a bezier curve (the contents of the b field of a SVG path) and outputs code that can be put in a PDF
#
# Usage:
#   python3 svg_bezier_to_pdf.py curve.svg
#
# Extract the curve from a svg with:
#   cat curve.svg | egrep ' d="[^"]*"' | egrep '".*"' > curve.bezier
# Documentation on how SVG represents bezier curves:
#   https://developer.mozilla.org/en/docs/Web/SVG/Tutorial/Paths

import sys

with open(sys.argv[1]) as f:
    data = f.read()
commands = data.split(" ")

points = []
for cmd in commands:
    if "," in cmd:
        points.append(list(map(float, cmd.split(","))))

start_point = -50, 200

print("{} {} m".format(*map(lambda x: round(x, 1), (start_point[0]+points[0][0], start_point[1]+points[0][1]))))
last_point = start_point[0]+points[0][0], start_point[1]+points[0][1]
for i in range(1, len(points)-2, 3):
    line = ""
    for point in points[i:i+3]:
        new_point = last_point[0]+point[0], last_point[1]-point[1]
        #new_point = point[0], point[1]
        line += "{} {} ".format(*map(lambda x: round(x, 1), new_point))
    last_point = new_point[0], new_point[1]
    line += "c"
    print(line)

#print(points)

